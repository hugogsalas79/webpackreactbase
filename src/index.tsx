import App from "./app";
import "bootstrap/dist/css/bootstrap.min.css";
import "./index.scss";
import ReactDOM from "react-dom";
import * as serviceWorker from "./serviceWorker";

ReactDOM.render(<App />, document.getElementById("app"));

serviceWorker.unregister();
