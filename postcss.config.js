module.exports = {
    plugins: [
        require('precss'),
        require('autoprefixer'),
        require('postcss-normalize'),
        require("postcss-import"),
        require('cssnano')({
            preset: 'default',
        }),
      [
        "postcss-preset-env",
      ],
    ],
  };