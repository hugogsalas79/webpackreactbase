const PnpWebpackPlugin = require('pnp-webpack-plugin');
const InterpolateHtmlPlugin = require('react-dev-utils/InterpolateHtmlPlugin');
const ModuleScopePlugin = require('react-dev-utils/ModuleScopePlugin');
const paths = require('./paths');
const ModuleNotFoundPlugin = require('react-dev-utils/ModuleNotFoundPlugin');
const { IgnorePlugin, DefinePlugin } = require('webpack')
const getClientEnvironment = require('./env');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const modules = require('./modules');

const isEnvProductionProfile = process.argv.includes('--profile');

const reactRefreshOverlayEntry = require.resolve(
  'react-dev-utils/refreshOverlayInterop'
);

const env = getClientEnvironment(paths.publicUrlOrPath.slice(0, -1));

module.exports = {
  entry: {
    app: paths.appIndexJs,
  },
  target: 'web',
  resolve: {
    extensions: paths.moduleFileExtensions
      .map(ext => `.${ext}`),
    mainFields: ['browser', 'module', 'main'],
    modules: ['node_modules', paths.appNodeModules].concat(
      modules.additionalModulePaths || []
    ),
    alias: {
      'react-native': 'react-native-web',
      ...(modules.webpackAliases || {}),
      ...(isEnvProductionProfile && {
        'react-dom$': 'react-dom/profiling',
        'scheduler/tracing': 'scheduler/tracing-profiling',
      })
    },
    plugins: [
      PnpWebpackPlugin,
      new ModuleScopePlugin(paths.appSrc, [
        paths.appPackageJson,
        reactRefreshOverlayEntry,
      ]),
    ]
  },
  resolveLoader: {
    plugins: [
      PnpWebpackPlugin.moduleLoader(module),
    ],
  },
  module: {
    strictExportPresence: true,
    rules: [
      { parser: { requireEnsure: false } },
      {
        test: /\.(js|mjs|ts|mts|jsx|tsx)$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader",
          options: {
            babelrc: true
          }
        },
      },
      {
        test: /\.css$/i,
        use: [
          "style-loader",
          "css-loader",
          "postcss-loader"
        ],
      },
      {
        test: [/\.ts$/, /\.tsx$/],
        exclude: /node_modules/,
        use: {
          loader: 'ts-loader',
          options: {
            transpileOnly: true
          }
        },

      },
      {
        test: /\.s[ac]ss$/i,
        use: [
          "style-loader",
          "css-loader",
          "postcss-loader",
          "resolve-url-loader",
          "sass-loader",
        ],
      },
      {
        loader: "url-loader",
        test: [/\.bmp$/, /\.gif$/, /\.jpe?g$/, /\.png$/],
        options: {
          emitFile: false,
          name: 'static/media/[name].[hash:8].[ext]',
        },
      },
      {
        test: /node_modules\/svelte\/.*\.mjs$/,
        resolve: {
          fullySpecified: false
        }
      },
      {
        loader: 'file-loader',
        test: /\.(woff2?|ttf|otf|eot|svg)$/,
        exclude: [/\.(js|mjs|jsx|ts|tsx)$/, /\.html$/, /\.json$/],
        options: {
          name: 'static/media/[name].[hash:8].[ext]',
        }
      }
    ]
  },
  plugins: [
    new InterpolateHtmlPlugin(HtmlWebpackPlugin, env.raw),
    new ModuleNotFoundPlugin(paths.appPath),
    new DefinePlugin(env.stringified),
    new IgnorePlugin(/^\.\/locale$/, /moment$/),
  ],
  performance: false,
};